/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.anvil;

/**
 * Based on information from
 * https://minecraft.gamepedia.com/Chunk_format#Tile_tick_format
 */

public class TileTick {
	String id; //The ID of the block; used to activate the correct block update procedure.
	int t; //The number of ticks until processing should occur. May be negative when processing is overdue.
	int p; //If multiple tile ticks are scheduled for the same tick, tile ticks with lower p will be processed first. If they also have the same p, the order is unknown.
	int x; //X position
	int y; //Y position
	int z; //Z position
	
	public TileTick(String id, int t, int p, int x, int y, int z) {
		this.id = id;
		this.t= t;
		this.p = p;
		this.x = x;
		this.y = y;
		this.z = z;
	}
}
