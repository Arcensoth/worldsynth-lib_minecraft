/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.module.anvil;

public class ChunkRegion {
	private final int startChunkX;
	private final int startChunkZ;
	private final int endChunkX;
	private final int endChunkZ;
	
	/**
	 * Constructs a chunk region containing the chunks containing the blocks in the specified area
	 * @param x block coordinate
	 * @param z block coordinate
	 * @param width in blocks
	 * @param lenght in blocks
	 */
	public ChunkRegion(long x, long z, long width, long lenght) {
		startChunkX = (int) Math.floor((double)x/16.0);
		startChunkZ = (int) Math.floor((double)z/16.0);
		endChunkX = (int) Math.floor((double)(x+width)/16.0);
		endChunkZ = (int) Math.floor((double)(z+lenght)/16.0);
	}
	
	public int getStartX() {
		return startChunkX;
	}
	
	public int getStartZ() {
		return startChunkZ;
	}
	
	public int getEndX() {
		return endChunkX;
	}
	
	public int getEndZ() {
		return endChunkZ;
	}
	
	public int getWidth() {
		return endChunkX - startChunkX + 1;
	}
	
	public int getLenght() {
		return endChunkZ - startChunkZ + 1;
	}
	
	public int chunkCount() {
		return getWidth() * getLenght();
	}
}
