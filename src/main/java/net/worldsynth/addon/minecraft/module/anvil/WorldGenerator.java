/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.module.anvil;

public enum WorldGenerator {
	DEFAULT ("default"),
	LARGEBIOMES ("largeBiomes"),
	FLAT ("flat"),
	AMPLIFIED ("amplified"),
	WORLDSYNTH ("worldsynth");
	
	private final String name;
	
	private WorldGenerator(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
